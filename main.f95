    PROGRAM polinomial_root

        use library

        implicit none

        ! Passo.
        real h
        parameter(h = 1.0e-5)

        ! Tolerância
        real e
        parameter(e = 2.0e-6)

        real x0 , x , y0 , y

        integer i

        x0 = -10
        x = x0

        ! O número de raízes é conhecidas a priori.
        i = 0
        do while( i < 3 )

            ! Uma raíz só aparece no intervalo em que o polinômio troca.
            ! de sinal nos valores dos extremos do intervalo.
            do while( p(x0)*p(x) .ge. 0 )
                x0 = x - h
                x  = x + h
            enddo

            ! Aqui encontramos um intervalo, vamos refinar para achar a raíz
            ! com o valor de tolerância.
            y0 = x0
            y = x
            do while( abs( p(y) ) .gt. e )

                if( abs( p(y) ) > abs( p(y0) ) )then
                    y = ( y + y0 )/2
                else if( abs( p(y) ) < abs( p(y0) ) )then
                    y0 = ( y + y0 )/2
                endif

            enddo

            write(*,"(A,I2,A,F12.6,A,F12.6,A,F12.6)" ) "raiz ", i+1, " :: ", y, " +/-", e, " | p(raiz) :: ", p(y)

            x0 = y
            x = x0 + h
            i = i + 1

        enddo

    END PROGRAM polinomial_root
