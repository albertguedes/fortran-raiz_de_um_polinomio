    MODULE library

        implicit none

        CONTAINS

            ! Polinômio em x.
            real FUNCTION p(x) RESULT(y)

                implicit none

                real a,b,x

                a = 3
                b = 2
                ! Dica: essa equacao é usada em criptografia ECC.
                y = x**3 + a*x + b 

                return

            END FUNCTION p

            ! Derivada do polinômio.
            real FUNCTION dp(x) RESULT(y)

                implicit none

                real x

                y = 4.6*x + 4.5

                return

            END FUNCTION dp

    END MODULE library
